FROM openjdk:8u111-jdk-alpine
RUN addgroup -g 1000 -S gitlab-runner && \
    adduser -u 1000 -S gitlab-runner -G gitlab-runner
USER gitlab-runner
VOLUME /tmp
ADD /target/ip-config-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
